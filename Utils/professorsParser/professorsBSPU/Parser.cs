﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace professorsBSPU
{
    public class Parser
    {
        private string _pattern;

        public Parser(string pattern)
        {
            _pattern = pattern;
        }

        public string GetMatch(string content)
        {        
            return Regex.Match(content, _pattern).Groups[1].Value;
        }

    }
}
