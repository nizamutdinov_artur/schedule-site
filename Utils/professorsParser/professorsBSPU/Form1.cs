﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace professorsBSPU
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }





        private void Form1_Load(object sender, EventArgs e)
        {

        string[] files =    Directory.GetFiles(Directory.GetCurrentDirectory() + "/source");
            foreach (var filePath in files)
            {
                string[] outputPathParts = filePath.Split('\\', '/');
                string destPath = String.Empty;
                
                for (int i = 0; i < outputPathParts.Count() - 2; i++)
                {
                    destPath += outputPathParts[i] + "/";
                }

                destPath += "output/" + outputPathParts[outputPathParts.Length - 1];

           File.WriteAllLines(destPath, GetInfoWithMEGACASE((File.ReadAllLines(filePath)),
               filePath    .Split('\\','/')[filePath.Split('\\', '/').Length-1]
               ));

            }

        }


        public string[] GetInfo(string[] links)
        {
            List<string> outputStrings = new List<string>();

            foreach (var link in links)
            {

                try
                {
                    string html = Downloader.Download(link);

                    string outputRow = String.Empty;
                    outputRow += ";";
                    outputRow += GetShortName(html) + ";";
                    outputRow += GetNamePairs(html) + ";";
                    outputRow += GetFacultyName(html) + ";";
                    outputRow += GetDivisionName(html) + ";";
                    outputRow += link + ";";
                    outputRow += GetRank(html);
                    outputStrings.Add(outputRow);
                }
                catch (Exception ex)
                {
                    
                }
            }

            return outputStrings.ToArray();
        }




        public string[] GetInfoWithMEGACASE(string[] links, string fileName)
        {
            List<string> outputStrings = new List<string>();

            foreach (var link in links)
            {

                try
                {
                    string html = Downloader.Download(link);

                    string outputRow = String.Empty;
                    outputRow += ";";
                    outputRow += GetShortName(html) + ";";
                    outputRow += GetNamePairs(html) + ";";

                    switch (fileName )
                    {
                        case "egf.txt": outputRow += "22;"; break;
                        case "fbf.txt": outputRow += "6;"; break;
                        case "ffk.txt": outputRow += "8;"; break;
                        case "fmf.txt": outputRow += "9;"; break;
                        case "fp.txt": outputRow += "7;"; break;
                        case "hgf.txt": outputRow += "10;"; break;
                        case "idp.txt": outputRow += "12;"; break;
                        case "ifomk.txt": outputRow += "4;"; break;
                        case "iipo.txt": outputRow += "2;"; break;
                        case "ip.txt": outputRow += "11;"; break;
                        case "ipoiit.txt": outputRow += "3;"; break;
                        case "sgf.txt": outputRow += "5;"; break;
                        default:throw new Exception("Ты обосрался");

                    }                                           
                    outputRow += GetDivisionName(html) + ";";
                    outputRow += link + ";";
                    outputRow += GetRank(html);
                    outputRow += ";15; 47; 4";
                    outputStrings.Add(outputRow);
                }
                catch (Exception ex)
                {
                   
                }
            }

            return outputStrings.ToArray();
        }

        private string GetShortName(string content)
        {
            Parser parser = new Parser(@"<span id='title'>([А-я\s]+?)\s<");
            string[] parts = parser.GetMatch(content).Split(' ');
            return parts[0] + " " + parts[1][0] + ". " + parts[2][0]+ ".";
        }

        private string GetNamePairs(string content)
        {
            Parser parser = new Parser(@"<span id='title'>([А-я\s]+?)\s<");
            string[] parts = parser.GetMatch(content).Split(' ');
            return parts[0] + ";" + parts[1] + ";" + parts[2];
        }

        private string GetFacultyName(string content)
        {
            Parser parser = new Parser(@"<tr id='field__4' ><td style='color:#888; width:250px;'>Факультет<\/td><td>([А-я\s\-]+?)<\/td>");
            return parser.GetMatch(content);
        }

        private string GetDivisionName(string content)
        {
            Parser parser = new Parser(@"<tr id='field__32' ><td style='color:#888; width:250px;'>Кафедра<\/td><td>\S+\s([\S\s]+?)<\/td>");
            return parser.GetMatch(content);
        }

        private string GetRank(string content)
        {
            Parser parser = new Parser(@"<tr id='field____2' ><td style='color:#888; width:250px;'>Ученая степень, звание<\/td><td>([\S\s^<]+?)<\/td>");
            return parser.GetMatch(content);
        }
    }
}
