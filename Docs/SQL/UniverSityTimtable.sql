-- MySQL Script generated by MySQL Workbench
-- 04/12/16 11:29:07
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bspu_schedule
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bspu_schedule
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bspu_schedule` DEFAULT CHARACTER SET utf8 ;
USE `bspu_schedule` ;

-- -----------------------------------------------------
-- Table `bspu_schedule`.`university`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`university` (
  `university_id` INT NOT NULL AUTO_INCREMENT,
  `university_short_name` VARCHAR(60) NOT NULL,
  `university_full_name` VARCHAR(250) NOT NULL,
  `university_description` MEDIUMTEXT NULL,
  PRIMARY KEY (`university_id`))
ENGINE = InnoDB
COMMENT = 'Университет.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`faculty`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`faculty` (
  `faculty_id` INT NOT NULL AUTO_INCREMENT,
  `faculty_university` INT NOT NULL,
  `faculty_short_name` VARCHAR(20) NOT NULL,
  `faculty_full_name` VARCHAR(120) NOT NULL,
  `faculty_adress` VARCHAR(45) NOT NULL,
  `faculty_campus` INT NULL,
  `faculty_description` MEDIUMTEXT NULL,
  PRIMARY KEY (`faculty_id`),
  INDEX `fk_faculty_university_idx` (`faculty_university` ASC),
  CONSTRAINT `fk_faculty_university`
    FOREIGN KEY (`faculty_university`)
    REFERENCES `bspu_schedule`.`university` (`university_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Факультет.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`rank`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`rank` (
  `rank_id` INT NOT NULL AUTO_INCREMENT,
  `rank_name` VARCHAR(45) NOT NULL,
  `rank_short_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`rank_id`))
ENGINE = InnoDB
COMMENT = 'Учёное звание.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`degree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`degree` (
  `degree_id` INT NOT NULL AUTO_INCREMENT,
  `degree_name` VARCHAR(50) NOT NULL,
  `degree_short` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`degree_id`))
ENGINE = InnoDB
COMMENT = 'Учёная степень.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`position`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`position` (
  `position_id` INT NOT NULL AUTO_INCREMENT,
  `position_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`position_id`))
ENGINE = InnoDB
COMMENT = 'Должность.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`professor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`professor` (
  `professor_id` INT NOT NULL AUTO_INCREMENT,
  `professor_short_name` VARCHAR(30) NOT NULL,
  `professor_first_name` VARCHAR(45) NOT NULL,
  `professor_second_name` VARCHAR(45) NOT NULL,
  `professor_middle_name` VARCHAR(45) NOT NULL,
  `professor_faculty_id` INT NOT NULL,
  `professor_division_name` VARCHAR(60) NULL,
  `professor_url` VARCHAR(30) NULL,
  `professor_rank` INT NOT NULL,
  `professor_degree` INT NOT NULL,
  `professor_position` INT NOT NULL,
  `professor_desription` MEDIUMTEXT NULL,
  PRIMARY KEY (`professor_id`),
  INDEX `fk_professor_rank1_idx` (`professor_rank` ASC),
  INDEX `fk_professor_degree1_idx` (`professor_degree` ASC),
  INDEX `fk_professor_position1_idx` (`professor_position` ASC),
  CONSTRAINT `fk_professor_rank1`
    FOREIGN KEY (`professor_rank`)
    REFERENCES `bspu_schedule`.`rank` (`rank_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professor_degree1`
    FOREIGN KEY (`professor_degree`)
    REFERENCES `bspu_schedule`.`degree` (`degree_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_professor_position1`
    FOREIGN KEY (`professor_position`)
    REFERENCES `bspu_schedule`.`position` (`position_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Преподаватель.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`division`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`division` (
  `division_id` INT NOT NULL AUTO_INCREMENT,
  `division_short_name` VARCHAR(20) NOT NULL,
  `division_full_name` VARCHAR(120) NOT NULL,
  `division_description` MEDIUMTEXT NULL,
  `division_university_id` INT NULL DEFAULT 1,
  PRIMARY KEY (`division_id`))
ENGINE = InnoDB
COMMENT = 'Кафедра.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`faculty_divisions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`faculty_divisions` (
  `fd_id` INT NOT NULL,
  `faculty_id` INT NOT NULL,
  `division_id` INT NOT NULL,
  INDEX `fk_table5_faculty1_idx` (`faculty_id` ASC),
  INDEX `fk_faculty_divisions_division1_idx` (`division_id` ASC),
  PRIMARY KEY (`fd_id`),
  CONSTRAINT `fk_table5_faculty1`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `bspu_schedule`.`faculty` (`faculty_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_faculty_divisions_division1`
    FOREIGN KEY (`division_id`)
    REFERENCES `bspu_schedule`.`division` (`division_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Связующая таблица факультета и кафедры.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`divisions_professors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`divisions_professors` (
  `dp_id` INT NOT NULL,
  `division_id` INT NOT NULL,
  `professor_id` INT NOT NULL,
  INDEX `fk_divisions_professors_division1_idx` (`division_id` ASC),
  INDEX `fk_divisions_professors_professor1_idx` (`professor_id` ASC),
  PRIMARY KEY (`dp_id`),
  CONSTRAINT `fk_divisions_professors_division1`
    FOREIGN KEY (`division_id`)
    REFERENCES `bspu_schedule`.`division` (`division_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_divisions_professors_professor1`
    FOREIGN KEY (`professor_id`)
    REFERENCES `bspu_schedule`.`professor` (`professor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Связующая таблица преподователя и кафедры.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`speciality`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`speciality` (
  `speciality_id` INT NOT NULL AUTO_INCREMENT,
  `speciality_faculty` INT NOT NULL,
  `speciality_faculty_name` VARCHAR(10) NOT NULL,
  `speciality_number` VARCHAR(45) NOT NULL,
  `speciality_short_name` VARCHAR(45) NOT NULL,
  `speciality_full_name` MEDIUMTEXT NOT NULL,
  `speciality_division` VARCHAR(45) NULL,
  `speciality_qualification` VARCHAR(45) NULL,
  `speciality_description` MEDIUMTEXT NULL,
  PRIMARY KEY (`speciality_id`),
  INDEX `fk_speciality_faculty1_idx` (`speciality_faculty` ASC),
  CONSTRAINT `fk_speciality_faculty1`
    FOREIGN KEY (`speciality_faculty`)
    REFERENCES `bspu_schedule`.`faculty` (`faculty_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Специальность.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`curriculum`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`curriculum` (
  `curriculum_id` INT NOT NULL AUTO_INCREMENT,
  `curriculum_speciality` INT NOT NULL,
  `curriculum_name` VARCHAR(45) NULL,
  PRIMARY KEY (`curriculum_id`),
  INDEX `fk_curriculum_speciality1_idx` (`curriculum_speciality` ASC),
  CONSTRAINT `fk_curriculum_speciality1`
    FOREIGN KEY (`curriculum_speciality`)
    REFERENCES `bspu_schedule`.`speciality` (`speciality_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Базовый и рабочие учебные планы.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`education`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`education` (
  `education_id` INT NOT NULL AUTO_INCREMENT,
  `education_key` VARCHAR(10) NOT NULL,
  `education_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`education_id`))
ENGINE = InnoDB
COMMENT = 'Учебная дисциплина.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`groups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`groups` (
  `group_id` INT NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(30) NOT NULL,
  `group_course` INT NOT NULL,
  `group_speciality` INT NOT NULL,
  `group_speciality_number` VARCHAR(30) NOT NULL,
  `group_size` INT NULL,
  `group_faculty_id` INT NOT NULL,
  PRIMARY KEY (`group_id`),
  INDEX `fk_group_speciality1_idx` (`group_speciality` ASC),
  INDEX `fk_group_faculty_id1_idx` (`group_faculty_id` ASC),
  CONSTRAINT `fk_group_speciality1`
    FOREIGN KEY (`group_speciality`)
    REFERENCES `bspu_schedule`.`speciality` (`speciality_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_faculty_id1`
    FOREIGN KEY (`group_faculty_id`)
    REFERENCES `bspu_schedule`.`faculty` (`faculty_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Группа.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`exam`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`exam` (
  `exam_id` INT NOT NULL AUTO_INCREMENT,
  `exam_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`exam_id`))
ENGINE = InnoDB
COMMENT = 'Тип контроля.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`schedule` (
  `schedule_id` INT NOT NULL AUTO_INCREMENT,
  `schedule_education` INT NOT NULL,
  `schedule_curriculum` INT NOT NULL,
  `schedule_total` INT NOT NULL,
  `schedule_lecture` INT NOT NULL,
  `schedule_seminar` INT NOT NULL,
  `schedule_lab` INT NOT NULL,
  `schedule_self` INT NOT NULL,
  `schedule_control` INT NOT NULL,
  `schedule_exam` INT NOT NULL,
  PRIMARY KEY (`schedule_id`),
  INDEX `fk_curriculum_education_education1_idx` (`schedule_education` ASC),
  INDEX `fk_curriculum_education_curriculum1_idx` (`schedule_curriculum` ASC),
  INDEX `fk_schedule_exam1_idx` (`schedule_exam` ASC),
  CONSTRAINT `fk_curriculum_education_education1`
    FOREIGN KEY (`schedule_education`)
    REFERENCES `bspu_schedule`.`education` (`education_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_curriculum_education_curriculum1`
    FOREIGN KEY (`schedule_curriculum`)
    REFERENCES `bspu_schedule`.`curriculum` (`curriculum_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_schedule_exam1`
    FOREIGN KEY (`schedule_exam`)
    REFERENCES `bspu_schedule`.`exam` (`exam_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Содержание базового учебного плана.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`burden`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`burden` (
  `burden_id` INT NOT NULL AUTO_INCREMENT,
  `burden_professor` INT NOT NULL,
  `burden_schedule` INT NOT NULL,
  `burden_group` INT NOT NULL,
  `burden_subs` INT NULL,
  PRIMARY KEY (`burden_id`),
  INDEX `fk_burden_professor1_idx` (`burden_professor` ASC),
  INDEX `fk_burden_schedule1_idx` (`burden_schedule` ASC),
  INDEX `fk_burden_group1_idx` (`burden_group` ASC),
  CONSTRAINT `fk_burden_professor1`
    FOREIGN KEY (`burden_professor`)
    REFERENCES `bspu_schedule`.`professor` (`professor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_burden_schedule1`
    FOREIGN KEY (`burden_schedule`)
    REFERENCES `bspu_schedule`.`schedule` (`schedule_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_burden_group1`
    FOREIGN KEY (`burden_group`)
    REFERENCES `bspu_schedule`.`groups` (`group_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Учебная нагрузка.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`classroom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`classroom` (
  `classroom_id` INT NOT NULL,
  `classroom_size` VARCHAR(45) NOT NULL,
  `classroom_computers` VARCHAR(45) NULL,
  `classroom_number` INT NOT NULL,
  `classroom_level` INT NULL,
  PRIMARY KEY (`classroom_id`))
ENGINE = InnoDB
COMMENT = 'Аудитория.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`campus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`campus` (
  `campus_id` INT NOT NULL,
  `campus_number` INT NOT NULL,
  `campus_levels` INT NOT NULL,
  `campus_isHostel` TINYINT(1) NOT NULL,
  PRIMARY KEY (`campus_id`))
ENGINE = InnoDB
COMMENT = 'Корпус, общежитие.';


-- -----------------------------------------------------
-- Table `bspu_schedule`.`timetable`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bspu_schedule`.`timetable` (
  `timetable_id` INT NOT NULL AUTO_INCREMENT,
  `timetable_burden` INT NOT NULL,
  `timetable_weekday` INT NOT NULL,
  `timetable_begin` INT NOT NULL,
  `timetable_end` INT NOT NULL,
  `timetable_order` INT NOT NULL,
  `timetable_classroom` INT NOT NULL,
  `campus_campus_id` INT NOT NULL,
  PRIMARY KEY (`timetable_id`, `campus_campus_id`),
  INDEX `fk_timetable_burden1_idx` (`timetable_burden` ASC),
  INDEX `fk_timetable_classroom1_idx` (`timetable_classroom` ASC),
  INDEX `fk_timetable_campus1_idx` (`campus_campus_id` ASC),
  CONSTRAINT `fk_timetable_burden1`
    FOREIGN KEY (`timetable_burden`)
    REFERENCES `bspu_schedule`.`burden` (`burden_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timetable_classroom1`
    FOREIGN KEY (`timetable_classroom`)
    REFERENCES `bspu_schedule`.`classroom` (`classroom_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timetable_campus1`
    FOREIGN KEY (`campus_campus_id`)
    REFERENCES `bspu_schedule`.`campus` (`campus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Расписание.';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
