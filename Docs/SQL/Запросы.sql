/* Поиск по столбцу в таблице */
UPDATE `speciality` SET `speciality_faculty` = REPLACE( speciality_faculty, '22', '2' ) ;

/* Подбор специальности в группах */
update speciality left join groups on speciality.speciality_number  = groups.group_speciality_number 
set groups.group_speciality = speciality.speciality_id;

/* Подбор факультетов к группам */
update groups left join speciality on groups.group_speciality  = speciality.speciality_id 
set groups.group_faculty_id = speciality.speciality_faculty;

/* Заполнение division_professors */
INSERT INTO divisions_professors  (division_id, professor_id)	
SELECT division.division_id, professor.professor_id FROM professor, division WHERE division.division_full_name = professor.professor_division_name INTO OUTFILE 'd:\out22.csv';

/* Проверка */
select professor_id, professor_short_name from professor where professor.professor_division_name != "";

/* Импорт CSV без проверки внещних ключей */
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET foreign_key_checks=0;
SET unique_checks=0;
load data local infile 'd:\output.csv' 
into table professor fields terminated by ';'
enclosed by '"'
lines terminated by '\n'

/* Сброс автоинкримента */
ALTER TABLE professor AUTO_INCREMENT=1;

Alter table test modify id int not null auto_increment 