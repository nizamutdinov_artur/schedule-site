<?php

Class Controller_Login extends Controller
{

	public function __construct($name)
	{
		parent::__construct($name);
	}

	public function Default_action()
	{
		$this->data = $this->model->Get();
		$this->view->GenerateWithContent($this->data);
	}

}