<?php


Class Controller_Timetable extends Controller
{

    public function __construct($name)
    {
        parent::__construct($name);
    }


    public function Default_action(array $args = null)
    {
        $this->model->SetTimeTableContent($args);

        $this->data = $this->model->Get();

        $this->view->GeneratePage($this->data);
    }
}