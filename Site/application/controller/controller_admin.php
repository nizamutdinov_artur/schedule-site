<?php

Class Controller_Admin extends Controller
{

	public function __construct($name, $action)
	{
		$this->name = $name;
		$this->action = $action;

		$this->IncludeModel();

		$this->view = new View();

		$this->view->head = 'application/head/head_'.($this->name).'.php';

		$this->view->navbar = DIR_VIEW_ADMIN.'navbar.tpl';

		$this->view->content = DIR_VIEW_ADMIN.($this->action.'.tpl');

		$this->data = $this->model->Get($action);


		// Немного хардкода :)
		switch ($action) {
			case "dashboard":
				$this->data["Title"] = "Основные сведения";
				break;
			case "groups":
				$this->data["Title"] = "Редактирование групп";
				break;
			case "professors":
				$this->data["Title"] = "Редактирование преподавателей";
				break;
			case "faculty":
				$this->data["Title"] = "Редактирование факультетов";
				break;
			case "timetable":
				$this->data["Title"] = "Редактирование расписания";
				break;
		}

	}

private function IncludeModel()
{
	$model_name = "model_".$this->name;

	$model_file = strtolower($model_name).'.php';
	$model_path = 'application/model/'.$model_file;

	if(file_exists($model_path))
	{
		include $model_path;

		$this-> model = new $model_name;
	}
}

	public function Default_action()
	{
		if (!file_exists($this->view->content)) $this->view->content = DIR_VIEW_ADMIN.'dashboard.tpl';

		header('Location: /login');

		$this->view->GenerateAdminPage($this->data);
	}

	public function dashboard ()
	{
		$this->Default_action();
	}


	public function faculty ()
	{
		$this->Default_action();
	}

	public function groups ()
	{
		$this->Default_action();
	}

	public function professors ()
	{
		$this->Default_action();
	}

	public function timetable ()
	{
		$this->Default_action();
	}

}