<?php

Class Controller_Api extends Controller
{

	public function __construct($name)
	{
		parent::__construct($name);
	}

	public function Default_action()
	{
		$this->data = $this->model->Get();
		$this->view->GenerateWithContent($this->data);
	}

	public function get_faculties ()
	{
		$this->model->GetFacultiesInJson();
		$this->Default_action();
	}

	public function get_groups ($args)
	{
		$this->model->GetGroupsInJson($args);
		$this->Default_action();
	}

	public function get_divisions ($args)
	{
		$this->model->GetDivisionInJson($args);
		$this->Default_action();
	}

	public function get_professors ($args)
	{
		$this->model->GetProfessorsInJson($args);
		$this->Default_action();
	}
}