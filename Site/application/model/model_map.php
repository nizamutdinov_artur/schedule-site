<?php

Class Model_Map extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "Расположение корпусов";
        $this->data['CurrentPage'] = 'Map';

        $this->SetStudyWeek();

        return $this->data;
    }

    private function SetStudyWeek ()
    {
        $timeOperations = new TimeOperation();

        $this->data["currentStudyWeek"]= $timeOperations->getStudyWeek(time());

    }


}
