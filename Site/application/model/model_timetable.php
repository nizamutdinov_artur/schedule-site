<?php

Class Model_Timetable extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "Расписание БГПУ";
        $this->data['CurrentPage'] = 'Schedule';

        $this->SetStudyWeek();

        return $this->data;
    }

    private function SetStudyWeek ()
    {
        $timeOperations = new TimeOperation();


        $this->data["currentStudyWeek"]= $timeOperations->getStudyWeek(time());

    }


    // Заглушка метода выдачи информации из базы в шаблон
    // Захардкодено в силу смены модели базы данных
    public function SetTimeTableContent ($args)
    {
        $file = DIR_VIEW."timetables/".$args["groups"].".tpl";

        if (file_exists($file)){
            $this->data["timetable"] = $file;
            return;
        }

        if ($args["faculty"] == "3"){
            $this->data["timetable"] = DIR_VIEW . "timetables/" ."lessonsExceed.tpl";
            return;
        }

            $this->data["timetable"] = DIR_VIEW . "timetables/" ."error.tpl";

    }

}
