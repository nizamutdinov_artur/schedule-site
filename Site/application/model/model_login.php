<?php

Class Model_Login extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "Форма входа";
        $this->data['CurrentPage'] = 'Login';


        return $this->data;
    }

}
