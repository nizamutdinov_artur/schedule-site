<?php

Class Model_About extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "О проекте";
        $this->data['CurrentPage'] = 'About';

        $this->SetStudyWeek();

        return $this->data;
    }

    private function SetStudyWeek ()
    {
        $timeOperations = new TimeOperation();

        $this->data["currentStudyWeek"]= $timeOperations->getStudyWeek(time());
    }

}