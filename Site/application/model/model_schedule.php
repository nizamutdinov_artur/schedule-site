<?php

Class Model_Schedule extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "Расписание БГПУ";
        $this->data['CurrentPage'] = 'Schedule';

        $this->SetStudyWeek();

        return $this->data;
    }

    private function SetStudyWeek ()
    {
        $timeOperations = new TimeOperation();


        $this->data["currentStudyWeek"]= $timeOperations->getStudyWeek(time());

    }


}
