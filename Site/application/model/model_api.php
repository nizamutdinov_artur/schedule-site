<?php

Class Model_Api extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "Api";
        $this->data['CurrentPage'] = 'Api';

        return $this->data;
    }

    public function GetFacultiesInJson()
    {
        $this->data['response'] =  $this->GetQueryResultInJson("SELECT * FROM faculty;");
    }

    public function GetGroupsInJson ($args)
    {
        $this->data['response'] =  $this->GetQueryResultInJson("SELECT group_id, group_name FROM groups
                                          WHERE group_faculty_id = {$args["faculty_id"]} AND group_course = {$args["course"]};");
    }

    public function GetDivisionInJson ($args)
    {
        $this->data['response'] =  $this->GetQueryResultInJson("SELECT d.division_id, division_short_name FROM faculty_divisions f,
                                          division d WHERE f.faculty_id = {$args["faculty_id"]} AND d.division_id = f.division_id;");
    }

    public function GetProfessorsInJson ($args)
    {
        $this->data['response'] =  $this->GetQueryResultInJson("SELECT p.professor_id, professor_short_name, p.professor_first_name, p.professor_second_name, p.professor_middle_name, p.professor_url, p.professor_desription  FROM divisions_professors dp,
                                          professor p WHERE division_id = {$args["division_id"]} AND dp.professor_id = p.professor_id;");
    }

    public function GetQueryResultInJson ($query)
    {
        $this->database->Open();
        $data = array();

        $result = $this->database->Query($query);

        while ($row = $result->fetch_array(MYSQLI_NUM))
        {
            array_push($data, $row);
        }

        return $this->json_encode_cyr($data);
    }


   private function json_encode_cyr($str) {
        $arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
            '\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
            '\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
            '\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
            '\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
            '\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
            '\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',
            '\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
        $arr_replace_cyr = array('А', 'а', 'Б', 'б', 'В', 'в', 'Г', 'г', 'Д', 'д', 'Е', 'е',
            'Ё', 'ё', 'Ж','ж','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н','О','о',
            'П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш',
            'Щ','щ','Ъ','ъ','Ы','ы','Ь','ь','Э','э','Ю','ю','Я','я');

       return str_replace($arr_replace_utf,$arr_replace_cyr, json_encode($str));
    }
}
