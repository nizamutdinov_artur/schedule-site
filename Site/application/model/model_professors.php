<?php

Class Model_Professors extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Get()
    {
        $this->data["Title"] = "Преподаватели";
        $this->data['CurrentPage'] = 'Professors';

        $this->SetStudyWeek();

        return $this->data;
    }

    private function SetStudyWeek ()
    {
        $timeOperations = new TimeOperation();

        $this->data["currentStudyWeek"]= $timeOperations->getStudyWeek(time());
    }

}