<?php

// DIR
define('DIR_APPLICATION', 'application/');
define('DIR_COMPONENTS', 'application/components/');
define('DIR_VIEW', 'application/view/');
define('DIR_VIEW_ADMIN', 'application/view/admin/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'bspu_schedule');


/*
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'us-cdbr-azure-central-a.cloudapp.net');
define('DB_USERNAME', 'b2e1c2fb212a64');
define('DB_PASSWORD', '02bab46b');
define('DB_DATABASE', 'pinky');
 */