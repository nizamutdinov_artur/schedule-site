<?php

/**
 * Class TimeOperation
 */
class TimeOperation
{
	public function dateDiffInDays($date1, $date2)
	{
		return $date1->diff($date2)->days;
	}

	public function getCurrentSemester()
	{
		return ((int)date("n") < 9) ? 2 : 1;
	}

	/**
	 * @param $currentDate TimeStamp
	 *
	 * @return int
	 */
	public function getStudyWeek($currentDate)
	{
		$holidaysWeekShift = 20;

		$startingDate = $this->getCurrentSemester() == 1 ?
				DateTime::createFromFormat('d/m/Y', "2/9/" . ((int)date("Y") - 1)) :
				DateTime::createFromFormat('d/m/Y', "11/1/" . date("Y"));

		$destinationTime = new DateTime('@' . (int)$currentDate);

		return $this->getCurrentSemester() == 1 ?
				floor($this->dateDiffInDays($startingDate, $destinationTime) / 7) :
				floor($this->dateDiffInDays($startingDate, $destinationTime) / 7) + $holidaysWeekShift;
	}

}