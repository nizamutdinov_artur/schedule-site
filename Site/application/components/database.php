<?php

class Database
{
    private $connection;

    /**
     * Database constructor.
     */
    public function __construct() {}

    public function Open ()
    {
        $this->connection = mysqli_connect(DB_HOSTNAME,DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die ('Could connect to database:' . mysql_error());
        $this->connection->set_charset("utf8");
    }

    public function Close()
    {
        if (!empty($this->connection))
            mysqli_close($this->connection);
    }

    public function Insert ($table,$names, $values)
    {
        if (!empty($this->connection))
            return $this->Query( "INSERT INTO ".$table." (".$names.") VALUES (".$values.")".";" );

        die("Insert command failure - connection isn't open");
    }

    public function Select ($names, $table, $conditions)
    {
        if (!empty($this->connection))
            return $this->Query("SELECT " . $names . " FROM " . $table . " " . $conditions . ";");

        die("Select comand failure - connection isn't open");
    }

    public function Query ($query)
    {
        $query = $this->EncodeSpecialSymbols($query);

        $response = mysqli_query($this->connection,$query );

        if ( gettype($response) == 'object' || $response == true )
            return $response;

            $error = mysqli_error($this->connection);
            return $error;
    }

    // Обработка SQL injection
    private function EncodeSpecialSymbols ($text)
    {
        $text = str_replace("&","&amp;",$text);
        $text = str_replace("<","&lt;",$text);
        $text = str_replace(">","&gt;",$text);
        $text = str_replace("\"","&quot;",$text);
        //$text = str_replace("'","&#x27;",$text);
        $text = str_replace("/","&#x2F;",$text);
        $text = str_replace("\\","&#92;",$text);

        return $text;
    }
}