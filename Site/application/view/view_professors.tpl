<div class="container">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="well">
            <form method="get" action="" role="form">

                <h3>Выбрать преподавателя</h3>
                <br>
                <div class="form-group">
                    <select id="facultyProfessorSelect" class="form-control" name="Факультет">
                        <option selected="selected" value="0">Факультет</option>
                    </select>
                </div>
                <div class="form-group">
                    <select id="divisionProfessorSelect" class="form-control" name="Кафедра">
                        <option selected="selected" value="0">Кафедра</option>
                    </select>
                </div>
                <div class="form-group" align="right">
                    <button id="showButton" type="submit" class="btn btn-primary">Показать</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">

            <div class="table-responsive" style="background-color: #f5f5f5; padding-bottom: 0;">
                <table class="table table-bordered">
                    <tbody id="professorsTable"></tbody>
                </table>
            </div>
        </div>
        <script src="/js/professor.js"></script>
    </div>