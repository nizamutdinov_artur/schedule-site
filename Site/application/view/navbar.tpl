<nav role="navigation" id="navbar-custom-blue"  class="navbar navbar-default navbar-fixed-top ">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" style="margin-left: 20px;"
            <?php if ($data["CurrentPage"] == "Schedule") echo( 'id="active"');?>href="/"><img class="brand-image" src="img/logo.png">Расписание</a>

            <button type="button" id="collapseButton" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li></li>
                <li><a <?php if ($data["CurrentPage"] == "Professors") echo( 'id="active"');?>    href="professors">Преподаватели</a></li>
                <li class="mediumDisplay">
                    <a <?php if ($data["CurrentPage"] == "Map") echo( 'id="active"');?>
                    href="map">Расположение корпусов</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="currentWeek mediumDisplay"><?=$data["currentStudyWeek"];?> учебная неделя</li>
            </ul>
        </div>
    </div>
</nav>