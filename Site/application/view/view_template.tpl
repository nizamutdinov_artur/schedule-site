<!DOCTYPE html>
<html lang="ru">
<head>

    <Title><?=$data['Title']?></Title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/icon.gif"/>
    <link href="/css/normalize.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,500,700&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <?require_once ('application/components/bootstrap.php');?>
    <!-- Main CSS -->
    <link href="/css/style.css" rel="stylesheet">
    <!-- Head -->
    <?php
    if (file_exists($this->head))
    { include_once $this->head;}?>

</head>

<body>

<div class="container">
        <!-- Navbar -->
        <?php
        if (file_exists($this->navbar))
        { include_once $this->navbar;}?>

        <div class="row">
            <div class="content-wrap">
            <!-- Content -->
            <?php
            if(file_exists($this->content))
            { include $this->content;} ?>
            </div>
        </div>

            <!-- Footer -->
        <div class="site-footer">
            <div class="site-footer-text">
                <?=(int)date("Y")?> &copy Расписание БГПУ<br> <a href="http://bspu.ru/">Сайт университета</a>
            </div>
    </div>

</div>



</body>
<script src="js/main.js"></script>
</html>