	<div class="container">


		<div class="col-sm-12 ">
			<div class="jumbotron well" style="text-align: center">
				<h3>Электронное расписание БГПУ им. М. Акмуллы</h3>
				<br>
				<!-- Large modal window with project description -->
				<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Подробнее о проекте</button>

				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header" style=" background-color: #2B587A; color:white;">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" ">&times;</span></button>
								<h4 class="modal-title" id="gridSystemModalLabel">Электронное расписание БГПУ</h4>
							</div>
							<div class="container">
								<div class="row">
									<div class="col-sm-12" style="margin: 30px;">
										<h4 style=" text-align: center;  line-height: 2">Проект разрабатывался в рамках конкурса «Электронное расписание БГПУ» <br> студентами 3 курса факультета ИПОиИТ.</h4>
									</div>

									<div class="col-sm-8 col-sm-offset-2" style="margin-top: 30px">
										<div class="well-lg well" style="border-radius: 0; border: 2px #777 solid">В настоящий момент проект находится на стадии разработки.</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="well">
				<form method="get" action="/timetable" role="form">
					<div class="form-group">
						<label for="faculties">Факультет</label>
						<select id="facultySelect" name="faculty" class="form-control">
							<option value="0" id="firstOption" selected="selected">Выберите факультет</option>
						</select>
					</div>
					<div class="form-group">
						<label for="course">Курс</label>
						<select id="courseSelect" name="course" class="form-control">
							<?php for ($i = 1; $i <= 4; $i++) echo ("<option value={$i}>{$i}</option>");?>
						</select>
					</div>
					<div class="form-group">
						<label for="groups">Группа</label>
						<select id="groupSelect" name="groups" class="form-control">
							<option value="0" id selected="selected">Выберите группу</option>
						</select>
					</div>

					<div class="form-group" style="margin-top:30px;">
						<input id="viewTimetable" type="submit" value="Посмотреть расписание" class="btn btn-primary">
					</div>
				</form>
			</div>
		</div>
	</div>
