<h3>ПИНФ-11-15</h3>

    <div class="table-responsive" style="background-color: #f5f5f5; padding-bottom: 0;">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th class="col-md-1">Время</th>
                <th class="col-md-4">Предмет</th>
                <th class="col-md-3">Преподаватель</th>
                <th class="col-md-1">Аудитория</th>
                <th class="col-md-2">Тип</th>
                <th class="col-md-1">Подгруппа</th>
            </tr>

            <tr><th colspan="7" class="dayHeader">Понедельник</th></tr>
			<tr>
                <td class="col-md-1">08:30 - 10:05</td>
                <td class="col-md-4">Практикум «Прикладное программное обеспечение»</td>
                <td class="col-md-3">Рамазанова Р.Р</td>
                <td class="col-md-1">лабораторная работа</td>
                <td class="col-md-2">209</td>
                <td class="col-md-1">2 подгруппа</td>
            </tr>
			
            <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Практикум «Прикладное программное обеспечение»</td>
                <td class="col-md-3">Рамазанова Р.Р</td>
                <td class="col-md-1">лабораторная работа</td>
                <td class="col-md-2">209</td>
                <td class="col-md-1">1 подгруппа</td>
            </tr>

            <tr><th colspan="7" class="dayHeader">Вторник</th></tr>
			<tr>
                <td class="col-md-1">08:30 - 10:05</td>
                <td class="col-md-4">Объектно-ориентированное программирование </td>
                <td class="col-md-3">Забихуллин Ф.З.</td>
                <td class="col-md-1">208</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">1 подгруппа</td>
            </tr>
			
			<tr>
                <td class="col-md-1">08:30 - 10:05</td>
                <td class="col-md-4">Иностранный язык </td>
                <td class="col-md-3">Васильева В.П.</td>
                <td class="col-md-1">101</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">2 подгруппа</td>
            </tr>
			
            <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Физическая культура</td>
                <td class="col-md-3">Бадгутдинов Р.Ф.</td>
                <td class="col-md-1">5 корпус</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>

			<tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Объектно-ориентированное программирование </td>
                <td class="col-md-3">Забихуллин Ф.З.</td>
                <td class="col-md-1">208</td>
                <td class="col-md-2">лекция</td>
                <td class="col-md-1">вся группа</td>
            </tr>
			<tr>
                <td class="col-md-1">14:05 - 15:40</td>
				<td class="col-md-4">Иностранный язык </td>
                <td class="col-md-3">Васильева В.П.</td>
                <td class="col-md-1">101</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">1 подгруппа</td>
            </tr>
			<tr>
                <td class="col-md-1">14:05 - 15:40</td>
                <td class="col-md-4">Объектно-ориентированное программирование </td>
                <td class="col-md-3">Забихуллин Ф.З.</td>
                <td class="col-md-1">208</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">2 подгруппа</td>
            </tr>
			
            <tr><th colspan="7" class="dayHeader">Среда</th></tr>
            <tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Информатика и проектирование</td>
                <td class="col-md-3">Картак В.М.</td>
                <td class="col-md-1">213</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
			<tr>
                <td class="col-md-1">14:05 - 15:40</td>
                <td class="col-md-4">Дискретная математика </td>
                <td class="col-md-3">Рамазанова Р.Р.</td>
                <td class="col-md-1">213</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>

            <tr><th colspan="7" class="dayHeader">Четверг</th></tr>
            <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Физическая культура</td>
                <td class="col-md-3">Бадгутдинов Р.Ф.</td>
                <td class="col-md-1">5 корпус</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>

			<tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Правоведение</td>
                <td class="col-md-3">Хайрулина Г.Х.</td>
                <td class="col-md-1">311</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
			
            <tr><th colspan="7" class="dayHeader">Пятница</th></tr>
           <tr>
                <td class="col-md-1">08:30 - 10:05</td>
                <td class="col-md-4">Математика(математический анализ)</td>
                <td class="col-md-3">Давлетов Д.Б.</td>
                <td class="col-md-1">311</td>
                <td class="col-md-2">лекция</td>
                <td class="col-md-1">вся группа</td>
            </tr>

			<tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Математика(математический анализ)</td>
                <td class="col-md-3">Давлетов Д.Б.</td>
                <td class="col-md-1">311</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
		</tbody>
        </table>
    </div>