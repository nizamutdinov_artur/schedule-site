 <h3>ИСИТ-11-15</h3>

    <div class="table-responsive" style="background-color: #f5f5f5; padding-bottom: 0;">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th class="col-md-1">Время</th>
                <th class="col-md-4">Предмет</th>
                <th class="col-md-3">Преподаватель</th>
                <th class="col-md-1">Аудитория</th>
                <th class="col-md-2">Тип</th>
                <th class="col-md-1">Подгруппа</th>
            </tr>

            <tr><th colspan="7" class="dayHeader">Понедельник</th></tr>
            <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Иностранный язык</td>
                <td class="col-md-3">Вахитова И.А.</td>
                <td class="col-md-1">102</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">1 подгруппа</td>
            </tr>
			
			<tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Башкирский язык </td>
                <td class="col-md-3">Хурамшина А.Р.</td>
                <td class="col-md-1">а/з</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
			
			<tr>
                <td class="col-md-1">14:00 - 15:40</td>
                  <td class="col-md-4">Иностранный язык</td>
                <td class="col-md-3">Вахитова И.А.</td>
                <td class="col-md-1">102</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">2 подгруппа</td>
            </tr>

            <tr><th colspan="7" class="dayHeader">Вторник</th></tr>
			<tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Физическая культура</td>
                <td class="col-md-3">Рылова Е.В.</td>
                <td class="col-md-1">5 корпус</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
			
            <tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Философия  </td>
                <td class="col-md-3">Михайличенко Д.Г.</td>
                <td class="col-md-1">а/з</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">вся группа</td>
            </tr>

			<tr>
                <td class="col-md-1">14:00 - 15:40</td>
                <td class="col-md-4">Компьютерная геометрия и графика  </td>
                <td class="col-md-3">Исхаков А.Р.</td>
                <td class="col-md-1">203</td>
                <td class="col-md-2">лекция</td>
                <td class="col-md-1">вся группа</td>
            </tr>
			
            <tr><th colspan="7" class="dayHeader">Среда</th></tr>
            <tr>
                <td class="col-md-1">08:30 - 10:05</td>
                <td class="col-md-4">Безопасность жизнедеятельности</td>
                <td class="col-md-3">Чуктурова Н.И.</td>
                <td class="col-md-1">314, 1 общ.</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
            <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Безопасность жизнедеятельности</td>
                <td class="col-md-3">Чуктурова Н.И.</td>
                <td class="col-md-1">314, 1 общ.</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>
            <tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Практикум «Сервисное программное обеспечение»  </td>
                <td class="col-md-3">Хасанов Р.Р.</td>
                <td class="col-md-1">204</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">1 подгруппа</td>
            </tr>

            <tr><th colspan="7" class="dayHeader">Четверг</th></tr>
            <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Физическая культура</td>
                <td class="col-md-3">Рылова Е.В.</td>
                <td class="col-md-1">5 корпус</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>

			<tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Практикум «Сервисное программное обеспечение»  </td>
                <td class="col-md-3">Хасанов Р.Р.</td>
                <td class="col-md-1">204</td>
                <td class="col-md-2">лабораторная работа</td>
                <td class="col-md-1">2 подгруппа</td>
            </tr>
			
            <tr><th colspan="7" class="dayHeader">Пятница</th></tr>
           <tr>
                <td class="col-md-1">10:15 - 11:50</td>
                <td class="col-md-4">Компьютерная геометрия и графика</td>
                <td class="col-md-3">Хакимова Е.А.</td>
                <td class="col-md-1">208</td>
                <td class="col-md-2">практическое занятие</td>
                <td class="col-md-1">вся группа</td>
            </tr>

			<tr>
                <td class="col-md-1">12:20 - 13:55</td>
                <td class="col-md-4">Компьютерная геометрия и графика  </td>
                <td class="col-md-3">Исхаков А.Р.</td>
                <td class="col-md-1">203</td>
                <td class="col-md-2">лекция</td>
                <td class="col-md-1">вся группа</td>
            </tr>
		</tbody>
        </table>
    </div>