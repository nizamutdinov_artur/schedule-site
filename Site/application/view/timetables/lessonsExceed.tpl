<div class="">
    <div class="alert jumbotron alert-info" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        Занятия для данной группы закончились
    </div>
</div>