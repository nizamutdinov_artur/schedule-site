
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Основные сведения</h1>
    </div>
</div><!--/.row-->


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Статистика</div>
            <div class="panel-body">
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="panel panel-blue panel-widget ">
                        <div class="row no-padding">
                            <div class="col-sm-3 col-lg-5 widget-left">
                            </div>
                            <div class="col-sm-9 col-lg-7 widget-right">
                                <div class="large">12</div>
                                <div class="text-muted">Факультетов</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="panel panel-orange panel-widget">
                        <div class="row no-padding">
                            <div class="col-sm-3 col-lg-5 widget-left">
                            </div>
                            <div class="col-sm-9 col-lg-7 widget-right">
                                <div class="large">42</div>
                                <div class="text-muted">Кафедры</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="panel panel-red panel-widget">
                        <div class="row no-padding">
                            <div class="col-sm-3 col-lg-5 widget-left">
                            </div>
                            <div class="col-sm-9 col-lg-7 widget-right">
                                <div class="large">649</div>
                                <div class="text-muted">Групп</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="panel panel-teal panel-widget">
                        <div class="row no-padding">
                            <div class="col-sm-3 col-lg-5 widget-left">
                            </div>
                            <div class="col-sm-9 col-lg-7 widget-right">
                                <div class="large">992</div>
                                <div class="text-muted">Преподаватели</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.col-->
</div><!-- /.row -->
