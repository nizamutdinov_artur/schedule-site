<div class="container">
    <div class="col-sm-8 col-sm-offset-2">
        <h3>Выбрать группу</h3>
        <div class="well">
            <form method="get" action="/timetable" role="form">
                <div class="form-group">
                    <label for="faculties">Факультет</label>
                    <select id="facultySelect" class="form-control">
                        <option value="" id="firstOption" selected="selected">Выберите факультет</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="course">Курс</label>
                    <select id="courseSelect" name="course" class="form-control">
                        <option value=1>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option><option value=5>5</option><option value=6>6</option>						</select>
                </div>
                <div class="form-group">
                    <label for="groups">Группа</label>
                    <select id="groupSelect" name="groups" class="form-control">
                        <option value="" id selected="selected">Выберите группу</option>
                    </select>
                </div>
                <div class="form-group">
                    <input id="viewTimetable" type="submit" value="Посмотреть" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Добавление группы</div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="alert alert-info">Поля, помеченные знаком <strong>*</strong>, обязательны для заполнения.</div>
                    <form role="form" method="post">
                        <div class="well container-fluid">
                            <div class="row-fluid">
                                <div class="col-sm-4">
                                    <div class="form-name">
                                        <label>Название группы</label>
                                        <input class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-speciality">
                                        <label>Специальность</label>
                                        <input class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-subs">
                                        <label>Кол-во подгрупп</label>
                                        <input class="form-control">
                                    </div>
                                </div>

                            </div>

                            <form method="get" action="/timetable" role="form">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="faculties">Факультет</label>
                                        <select id="facultySelect" class="form-control">
                                            <option value="" id="firstOption" selected="selected">Выберите факультет</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="course">Курс</label>
                                        <select id="courseSelect" name="course" class="form-control">
                                            <option value=1>1</option><option value=2>2</option><option value=3>3</option><option value=4>4</option><option value=5>5</option><option value=6>6</option>						</select>
                                    </div>
                                </div>

                                <div align="right">
                                    <button type="submit" class="btn btn-primary">Добавить</button>
                                </div>
                            </form>


                    </form>
                </div>

                <div class="col-md-6">


                </div>

            </div>
        </div>
    </div>
    <!-- /.col-->
</div>

<script src="../js/main.js"></script>
