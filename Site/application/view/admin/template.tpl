<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$data["Title"]?></title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/datepicker3.css" rel="stylesheet">
    <link href="../css/admin/styles.css" rel="stylesheet">

    <!--Icons-->
    <script src="../js/lumino.glyphs.js"></script>

    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->

    <?php
    if (file_exists($this->head))
    { include_once $this->head;}?>

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>Панель </span>администратора</a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Администратор <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
                        <li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
                        <li><a href="#"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">

    <ul class="nav menu" style="margin-top: 20px">
        <li><a href="/admin/dashboard"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Основные сведения</a></li>
        <li><a href="/admin/timetable"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Расписания</a></li>
        <li><a href="/admin/professors"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-male-user"></use></svg> Преподаватели</a></li>
        <li><a href="/admin/groups"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Группы</a></li>
        <li><a href="/admin/faculty"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-plus-sign"></use></svg> Факультеты</a></li>


        <li role="presentation" class="divider"></li>
        <li><a href="/login"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Страница Входа</a></li>
    </ul>

</div><!--/.sidebar-->

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li class="active"><?=$data["Title"]?></li>
        </ol>
    </div><!--/.row-->


    <?php
    if(file_exists($this->content))
    { include $this->content;}
    ?>




</div>


</body>

</html>
