
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Расписание</h1>
        </div>
    </div><!--/.row-->


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <h3 class="panel-heading">Добавление предмета</h3>
                <div class="panel-body">
                    <div class="well">
                        <form method="get" action="/admin/schedule-edir" role="form">
                            <div class="form-lesson-type">
                                <label for="lesson-type">Тип занятия:</label>
                                <select id="facultySelect" class="form-control">
                                    <option value="" id="firstOption" selected="selected">Выберите тип занятия</option>
                                    <option value="616">Лекция</option>
                                    <option value="616">Практическое занятие</option>
                                    <option value="616">Семинар</option>
                                </select>
                            </div>

                            <div class="form-lesson">
                                <label for="lesson">Предмет</label>
                                <input type="text" id="lesson" name="lesson" class="form-control ui-autocomplete-input" required="" autocomplete="off">
                            </div>

                            <div class="form-classroom">
                                <label for="classroom">Аудитория</label>
                                <input type="text" id="classroom" name="classroom" class="form-control ui-autocomplete-input" required="" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="group">Группа</label>
                                <input type="text" id="group" name="group" class="form-control ui-autocomplete-input" required="" autocomplete="off">
                            </div>

                            <div class="form-weekday">
                                <label for="weekday">Тип занятия:</label>
                                <select id="weekday" class="form-control">
                                    <option value="" id="firstOption" selected="selected">Выберите день</option>
                                    <option value="1">Понедельник</option>
                                    <option value="2">Вторник</option>
                                    <option value="3">Среда</option>
                                    <option value="4">Четверг</option>
                                    <option value="5">Пятница</option>
                                    <option value="6">Суббота</option>
                                </select>
                            </div>

                            <div class="form-time">
                                <label for="time">Время:</label>
                                <select id="time" class="form-control">
                                    <option value="" id="firstOption" selected="selected">Выберите время</option>
                                    <option value="1">08:30 - 10:05</option>
                                    <option value="2">10:15 - 11:50</option>
                                    <option value="3">12:20 - 13:55</option>
                                    <option value="4">14:00 - 15:40</option>
                                    <option value="5">15:45 - 17:30</option>
                                    <option value="6">17:30 - 19:05</option>
                                </select>
                            </div>

                            <div class="form-time">
                                <label for="sub-group">Подгруппа:</label>
                                <select id="sub-group" class="form-control">
                                    <option value="" id="firstOption" selected="selected">Выберите подгруппу</option>
                                    <option value="1">Вся</option>
                                    <option value="2">1 подгруппа</option>
                                    <option value="3">2 подгруппа</option>
                                </select>
                            </div>

                            <div class="form-repeat">
                                <label for="repeat">Повтор (введите диапазон недель)</label>
                                <input type="text" id="repeat" name="repeat" class="form-control ui-autocomplete-input" required="" autocomplete="off">
                            </div>

                        </form>
                        <br>
                        <input type="submit" value="Добавить пару" class="btn btn-primary">
                    </div>
                </div>
        </div><!-- /.col-->
    </div><!-- /.row -->



