    <div class="col-sm-8 col-sm-offset-2">
        <div class="well">
            <form method="get" action="" role="form">

                <h3>Выбрать преподавателя</h3>
                <br>
                <div class="form-group">
                    <select id="facultyProfessorSelect" class="form-control" name="Факультет">
                        <option selected="selected" value="0">Факультет</option>
                    </select>
                </div>
                <div class="form-group">
                    <select id="divisionProfessorSelect" class="form-control" name="Кафедра">
                        <option selected="selected" value="0">Кафедра</option>
                    </select>
                </div>
                <div class="form-group" align="right">
                    <button id="showButton" type="submit" class="btn btn-primary">Показать</button>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-1">

        </div>
        <div class="col-sm-10 col-sm-offset-1">

            <div class="table-responsive" style="background-color: #f5f5f5; padding-bottom: 0;">
                <table class="table table-bordered">
                    <tbody id="professorsTable">
                    </tbody></table>
            </div>
            <script src="../js/professor.js"></script>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Добавление преподавателя</div>
                        <div class="panel-body">
                            <div class="container-fluid">
                                <div class="alert alert-info">Поля, помеченные знаком <strong>*</strong>, обязательны для заполнения.</div>
                                <form role="form" method="post">
                                    <div class="well container-fluid">
                                        <div class="row-fluid">
                                            <div class="col-sm-4">
                                                <div class="form-name">
                                                    <label>Фамилия*</label>
                                                    <input class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-name">
                                                    <label>Имя*</label>
                                                    <input class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-second-name">
                                                    <label>Отчество*</label>
                                                    <input class="form-control">
                                                </div>
                                            </div>

                                        </div>

                                            <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Факультет</label>
                                            <select id="facultyProfessorSelect" class="form-control" name="Факультет">
                                                <option selected="selected" value="0">Факультет</option>
                                            </select>
                                        </div>
                                        </div>
                                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Кафедра</label>
                                            <select id="divisionProfessorSelect" class="form-control" name="Кафедра">
                                                <option selected="selected" value="0">Кафедра</option>
                                            </select>
                                        </div>
                                                    </div>


                                        <div class="row-fluid">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label>Информация</label>
                                                    <textarea class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div align="right">
                                        <button type="submit" class="btn btn-primary">Добавить</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-6">


                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.col-->
