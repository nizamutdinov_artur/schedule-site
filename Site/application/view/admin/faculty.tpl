<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Факультеты</h1>
    </div>
</div>
<!--/.row-->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Список факультетов</div>
            <div class="panel-body">
                <div class="bootstrap-table">
                    <div class="fixed-table-toolbar"></div>
                    <div class="fixed-table-container">
                        <div class="fixed-table-header">
                            <table></table>
                        </div>
                        <div class="fixed-table-body">
                            <div style="top: 37px; display: none;" class="fixed-table-loading">Loading, please wait…</div>
                            <table class="table table-hover" data-toggle="table" data-url="tables/data2.json">
                                <thead>
                                <tr>
                                    <th style="text-align: right; ">
                                        <div class="th-inner ">ID</div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner ">Название</div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner ">Полное название</div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner ">Адрес</div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner ">Номер корпуса</div>
                                        <div class="fht-cell"></div>
                                    </th>
                                    <th style="">
                                        <div class="th-inner ">Описание</div>
                                        <div class="fht-cell"></div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbody-faculty">

                                </tbody>
                            </table>
                        </div>
                        <div class="fixed-table-pagination"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Добавление факультета</div>
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="alert alert-info">Поля, помеченные знаком <strong>*</strong>, обязательны для заполнения.</div>
                    <form role="form" method="post">
                        <div class="well container-fluid">
                            <div class="row-fluid">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Название*</label>
                                        <input class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Полное название*</label>
                                        <input class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Адрес*</label>
                                        <input class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Описание</label>
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div align="right">
                            <button type="submit" class="btn btn-primary">Добавить</button>
                        </div>
                    </form>
                </div>

                <div class="col-md-6">


                </div>

            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row -->

</div>

<script src="../js/admin/faculty.js"></script>