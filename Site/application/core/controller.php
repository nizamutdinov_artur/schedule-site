<?php

Class Controller
{
    public $model;
    public $view;

    public $name;
    public $action;

    protected $data;

    public function __construct($name)
    {
        $this->name = $name;

        $this->IncludeModel();

        $this->view = new View();

        $this->view->head = 'application/head/head_'.($this->name).'.php';

        $this->view->navbar = 'application/view/navbar.tpl';

        $this->view->content = 'application/view/view_'.($this->name).'.tpl';
    }

    private function IncludeModel()
    {
        $model_name = "model_".$this->name;

        $model_file = strtolower($model_name).'.php';
        $model_path = 'application/model/'.$model_file;

        if(file_exists($model_path))
        {
            include $model_path;

            $this-> model = new $model_name;
        }
    }

    public function Default_action()
    {
        $this->data = $this->model->Get();

        $this->view->GeneratePage($this->data);
    }

    public function ErrorMessage ($message)
    {

    }

}