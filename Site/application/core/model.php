<?php

Class Model
{
    protected $data;
    protected $database;

    public function __construct()
    {
        $this->data = array();
        $this->database = new Database();

    }

    public function Get()
    {
        return $this->data;
    }

    public function Set ($key, $value)
    {
        $this->data[$key] = $value;
    }

}