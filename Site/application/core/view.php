<?php

Class View
{
    public $head;
    public $content;
    public $navbar;

    public function GeneratePage( $data)
    {
        include DIR_VIEW.'view_template.tpl';
    }

    public function GenerateWithContent ($data)
    {
        include $this->content;
    }

    public function GenerateAdminPage ($data)
    {
        include DIR_VIEW_ADMIN."template.tpl";
    }
}