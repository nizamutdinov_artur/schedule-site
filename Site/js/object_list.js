// Группы объектов
var groups = [
    {
        name: "Корпуса",
        style: "islands#blueIcon",
        items: [
            {
                center: [ 54.72399, 55.9476],
                iconContent: "1",
                name: "1 корпус"
            },
            {
                center: [54.72318, 55.94843],
                iconContent: "2",
                name: "2 корпус"
            },
            {
                center: [54.72424, 55.94866],
                iconContent: "3",
                name: "3 корпус"
            },
            {
                center: [54.71797, 55.9605],
                iconContent: "4",
                name: "4 корпус"
            },
            {
                center: [54.73277, 55.92945],
                iconContent: "5",
                name: "5 корпус"
            },
            {
                center: [54.72368, 55.94991],
                iconContent: "6",
                name: "6 корпус"
            },
            {
                center: [54.72392, 55.95029],
                iconContent: "7",
                name: "7 корпус"
            },
            {
                center: [54.72465, 55.94994],
                iconContent: "8",
                name: "8 корпус"
            },
            {
                center: [54.724835,55.996629],
                iconContent: "9",
                name: "9 корпус"
            },
            {
                center: [54.73134, 55.93495],
                iconContent: "10",
                name: "10 корпус"
            },
            {
                center: [54.698163, 55.983880],
                iconContent: "11",
                name: "11 корпус"
            },
            {
                center: [54.745070, 55.956293],
                iconContent: "12",
                name: "12 корпус"
            },
            {
                center: [54.72352, 55.95089],
                iconContent: "13",
                name: "13 корпус"
            }
        ]},
    {
        name: "Общежития",
        style: "islands#greenIcon",
        items: [
            {
                center: [54.72621, 55.93276],
                iconContent: "1",
                name: "Общежитие №1"
            },
            {
                center: [54.727285,55.931567],
                iconContent: "2.",
                name: "Общежитие №2"
            },
            {
                center: [54.72702, 55.93329],
                iconContent: "3",
                name: "Общежитие №3"
            },
            {
                center: [54.72758, 55.93268],
                iconContent: "4",
                name: "Общежитие №4"
            },
            {
                center: [54.7282, 55.93296],
                iconContent: "5",
                name: "Общежитие №5"
            },
            {
                center: [54.72706, 55.93474],
                iconContent: "6",
                name: "Общежитие №6"
            }
        ]}
];

ymaps.ready(init);

function init() {

    // Создание экземпляра карты.
    var myMap = new ymaps.Map('map', {
            center: [54.72318, 55.94843],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        }),
    // Контейнер для меню.
        menu = $('<ul class="menu"></ul>');

    myMap.geoObjects.events.add('click', function (e) {
        var geoObject = e.get('target');
        var projection = myMap.options.get('projection');
        var position = geoObject.geometry.getCoordinates();
        var position_global_px = myMap.converter.pageToGlobal(projection.fromGlobalPixels(position, myMap.getZoom()));
        var position_local_px = myMap.converter.globalToPage(projection.toGlobalPixels(position,myMap.getZoom()));
        myMap.setGlobalPixelCenter([position_global_px[0] + position_local_px[0], position_global_px[1] + position_local_px[1]]);
    }, this);

    for (var i = 0, l = groups.length; i < l; i++) {
        createMenuGroup(groups[i]);
    }

    function createMenuGroup (group) {
        // Пункт меню.
        var menuItem = $('<li><a class="menu-group" href="#">' + group.name + '</a></li>'),
        // Коллекция для геообъектов группы.
            collection = new ymaps.GeoObjectCollection(null, { preset: group.style }),
        // Контейнер для подменю.
            submenu = $('<ul class="submenu"></ul>');

        // Добавляем коллекцию на карту.
        myMap.geoObjects.add(collection);

        // Добавляем подменю.
        menuItem
            .append(submenu)
            // Добавляем пункт в меню.
            .appendTo(menu)
            // По клику удаляем/добавляем коллекцию на карту и скрываем/отображаем подменю.
            .find('a')
            .toggle(function () {
                myMap.geoObjects.remove(collection);
                submenu.hide();
            }, function () {
                myMap.geoObjects.add(collection);
                submenu.show();
            });
        for (var j = 0, m = group.items.length; j < m; j++) {
            createSubMenu(group.items[j], collection, submenu);
        }
    }

    function createSubMenu (item, collection, submenu) {
        // Пункт подменю.
        var submenuItem = $('<li><a href="#">' + item.name + '</a></li>'),
        // Создаем метку.
            placemark = new ymaps.Placemark(item.center, { iconContent: item.iconContent, balloonContent: item.name });

        // Добавляем метку в коллекцию.
        collection.add(placemark);
        // Добавляем пункт в подменю.
        submenuItem
            .appendTo(submenu)
            // При клике по пункту подменю открываем/закрываем баллун у метки.
            .find('a')
            .toggle(function () {
                myMap.setZoom( 17 );
                placemark.balloon.open();

            }, function () {
                myMap.setZoom( 17 );
                placemark.balloon.close();
            });
    }

    // Добавляем меню в тэг BODY.
    menu.appendTo($('#map-left-menu'));
    // Выставляем масштаб карты чтобы были видны все группы.
    myMap.setBounds(myMap.geoObjects.getBounds());
}