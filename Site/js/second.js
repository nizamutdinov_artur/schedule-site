/**
 * Created by pinky on 08.04.2016.
 */

$("#search_submit").on('click', function()
{
	return false;
});

function OnHintClick ()
{
	($("#search").is(":visible") == true) ?
			(function ()
			{
				$("#search").hide();
				$("#combo-container").show();
				$("#search-box").css("width", "60%");
				$(this).html("Или же вернитесь к поиску");
			}()) :
			(function ()
			{
				$("#search").show();
				$("#combo-container").hide();
				$("#search-box").css("width", "80%");
				$(this).html("Или вы можете найти группу из списка");
			}())
}

(function() {
			$.getJSON( "http://schedule.bspu.ru/api/get_faculties", {}
			).done(function( data ) {
				$.each(data, function (key, value){
					$("#facultySelect").append("<option value="+ value[0] + ">"+ value[2]+"</option>");
				});
			});
		}
)();

$("#hint").on("click", function() { OnHintClick();});


OnHintClick();

$("#facultySelect, #courseSelect").on("change", function()
{
	if ( $("#facultySelect").val() == 0 || $("#courseSelect").val() == 0)
		return;

	$.getJSON(
			"http://schedule.bspu.ru/api/get_groups?"+
			"faculty_id="+$("#facultySelect").val()+ "&" +
			"course="+$("#courseSelect").val(), {}
	).done(function( data ) {
		$("#groupSelect").empty();
		$("#groupSelect").append("<option selected=\"selected\" value=\"0\">Группа</option>");

		$.each(data, function (key, value){
			$("#groupSelect").append("<option value="+ value[0] + ">"+ value[1]+"</option>");
		});
	});
});


function dump(obj) {
	var out = '';
	for (var i in obj) {
		out += i + ": " + obj[i] + "\n";
	}
	$("body").append(out);
}
