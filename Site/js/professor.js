(function() {
        $.getJSON( "/api/get_faculties", {}
        ).done(function( data ) {
            $.each(data, function (key, value){
                $("#facultyProfessorSelect").append("<option value="+ value[0] + ">"+ value[2]+"</option>");
            });
        });
    }
)();


$("#facultyProfessorSelect").on("change", function()
{
    if ( $("#facultyProfessorSelect").val() == 0)
        return;

    $.getJSON(
        "/api/get_divisions?"+
        "faculty_id="+$("#facultyProfessorSelect").val(), {}
    ).done(function( data ) {
        $("#divisionProfessorSelect").empty();
        $("#divisionProfessorSelect").append("<option selected=\"selected\" value=\"0\">Кафедра</option>");

        $.each(data, function (key, value){
            $("#divisionProfessorSelect").append("<option value="+ value[0] + ">"+ value[1]+"</option>");
        });
    });

});


$("#showButton").on("click", function(){
    $.getJSON("/api/get_professors?division_id="+ $("#divisionProfessorSelect").val(), {}
    ).done(function( data ) {
        $("#professorsTable").empty();
        $("#professorsTable").append("<tr>"+
        "<th class=\"col-md-1\">Фамилия</th>"+
        "<th class=\"col-md-1\">Имя</th>"+
        "<th class=\"col-md-1\">Отчество</th>"+
        "<th class=\"col-md-2\">Страница на bspu.ru</th>"+
        "<th class=\"col-md-3\">Дополнительная информация</th>"+
        "</tr>");

        $.each(data, function (key, value){
            $("#professorsTable").append("<tr></tr><td class=\"col-md-2\">"+value[2]+"</td>"+
                "<td class=\"col-md-1\">"+value[3]+"</td>"+
                "<td class=\"col-md-2\">"+value[4]+"</td>"+
                "<td class=\"col-md-3\"><a href=\""+value[5]+"\" target=\"_blank\">"+value[5]+"</a></td>"+
                "<td class=\"col-md-4\">"+value[6]+"</td></tr>");
        });
    });
    return false;
});


